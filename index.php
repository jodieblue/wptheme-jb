<?php get_header(); ?>


<div class="content">
<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
    <ul class="uk-slideshow-items">
      <?php while ( have_posts() ) : the_post(); ?>
        <li>
            <img src="<?php echo get_the_post_thumbnail( '', 'cover' ); ?>; ?>" alt="" uk-cover>
            <article class="article-content uk-overlay uk-light uk-position-bottom">
              <h1 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
              <div class="article-meta" >
              <span><?php the_time('n / j, Y'); ?></span><span> / </span>
              <span><?php the_category(' , '); ?></span><span> / </span>
              <span><?php the_tags('', ' , ',
              ''); ?></span>
              </div>
              <div class="clearfix"></div>
            </article>
          </li>
        <?php endwhile; ?>
    </ul>

    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
</div>

<div class="sidebar">
<?php get_sidebar(); ?>
</div>

</div>
<?php get_footer(); ?>