<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset');?>" />
<title><?php
if (is_home()) {
bloginfo('name');
echo ' - ';
bloginfo('description');
} else {
wp_title(' - ', true, 'right');
bloginfo('name');
} ?></title>
<?php wp_head(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href="<?php bloginfo('template_directory') ?>/style.css" media="screen" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_directory') ?>/assets/js/uikit.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/assets/js/uikit-icons.min.js"></script>
<body>
<div class="container">
<header >
    
    <nav class="uk-navbar-container" uk-navbar>
        <div class="uk-navbar-left">    
            <a class="uk-navbar-item uk-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                <?php wp_nav_menu( array( 
                    'theme_location' => 'primary-menu',
                    'items_wrap'     => '<ul class="uk-navbar-nav"><li id="item-id"></li>%3$s</ul>'
                ) ); ?>
        </div>
    </nav>
</header>
<?php
register_nav_menus(
array(
'primary-menu' => __( 'MainMenu' )
)
);
?>